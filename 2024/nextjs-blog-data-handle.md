# blog

```ts
import fs from 'fs';
import path from 'path';
import matter from 'gray-matter';

const CATEGORIES = ['diary', 'rust', 'backend', 'frontend', 'server'];
const REG_DATE = /\d{4}-\d{2}-\d{2}/i;

const content_path = path.join(process.cwd(), 'src/content');

export interface Node {
  content?: string;
  isEmpty?: boolean;
  excerpt?: string;
  file_path?: string;
  data?: {
    title?: string;
    description?: string;
    date?: Date;
    taxonomies?: {
      categories?: string[];
      tags?: string[];
    };
  };
}

export const getAllNodes = () => {
  const all_matter_results: Node[] = [];

  fs.readdirSync(content_path)?.filter((item) => {
    return CATEGORIES?.includes(item);
  })?.forEach((cat) => {
    const category_path = path.join(content_path, cat);
    // 遍历文件
    fs.readdirSync(category_path)?.filter((item) => {
      return !item?.startsWith('_index.md');
    }).forEach((fileName) => {
      const file_path = path.join(category_path, fileName);
      const file_contents = fs.readFileSync(file_path, 'utf-8');

      const matterResults = matter(file_contents);
      const slugDate = getSlugDate(fileName);

      all_matter_results.push({
        ...matterResults,
        file_path,
        data: {
          ...(matterResults?.data || {}),
          date: matterResults?.data?.date || slugDate,
        },
      });
    });
  });

  return all_matter_results;
};

export interface ListDataOptions {
  category?: string;
  tag?: string;
  page?: number;
  pageSize?: number;
  orderBy?: string;
  order?: 'asc' | 'desc';
}

export const getNodes = (options: ListDataOptions = {}) => {
  const allNodes = getAllNodes();
  let validNodes = allNodes;

  validNodes = validNodes?.filter((item) => {
    if (options?.category) {
      return item?.data?.taxonomies?.categories?.includes(options?.category);
    }

    if (options?.tag) {
      return item?.data?.taxonomies?.tags?.includes(options?.tag);
    }

    return true;
  });

  if (options?.orderBy === 'date') {
    validNodes = validNodes.sort((a, b) => {
      let aDate = a?.data?.date?.valueOf() || 0;
      let bDate = b?.data?.date?.valueOf() || 0;

      if (options?.order === 'asc') {
        return aDate - bDate;
      } else {
        return bDate - aDate;
      }
    });
  }

  if (options?.pageSize) {
    validNodes = validNodes?.slice(0, options.pageSize);
  }

  return validNodes;
};

/**
 * 获取文件名中的日期
 */
export const getSlugDate = (slug: string): Date | undefined => {
  const matches = slug?.match(REG_DATE);

  if (matches?.[0]) {
    return new Date(matches?.[0]);
  }

  return undefined;
};
```


api:

```ts
export const generateStaticParams = async () => {
  const pages = [1,2,3];
  return pages?.map((item) => {
    return {
      page: `${item}.json`,
    };
  });
};

export const GET = async (req: Request, ctx: any) => {
  const param_page = ctx?.params?.page;
  const data = {
    name: 'alex',
    param_page,
  };
  return Response.json(data);
};

```