在 RESTful 风格的 Web 应用中，一个 Controller 类常常会包含对应资源的所有 CRUD 操作。而在 GraphQL 中，由于其查询语言和响应式数据的特点，通常采用 Schema 和 Resolver 的方式来组织资源相关的操作。

在 async-graphql（或者任何其他 GraphQL 实现）中，组件文件结构可以这样设计以保持清晰且合理：

## 1.定义Schema 创建一个名为 schema.graphql 的文件来编写你的 GraphQL Schema 语句，描述你应用中的类型、查询和 mutation。


```
schema {
    query: Query
    mutation: Mutation
  }

  type Book {
    id: ID!
    title: String!
    author: String!
    // 其他字段...
  }

  type Query {
    book(id: ID!): Book
    books: [Book!]!
  }

  type Mutation {
    createBook(title: String!, author: String!): Book!
    updateBook(id: ID!, title: String, author: String): Book
    deleteBook(id: ID!): Boolean
  }
```

## 2.创建Resolver 创建一个或多个 Rust 文件用于实现上述 Schema 中定义的 resolver 函数。例如，可以创建 resolvers.rs 或按资源分目录，如 book/resolvers.rs。

```rust
use async_graphql::*;

#[derive(SimpleObject)]
struct Book {
    id: i32,
    title: String,
    author: String,
    // 其他字段的实现...
}

struct Query;

#[Object]
impl Query {
    async fn book(&self, ctx: &Context<'_>, id: i32) -> FieldResult<Option<Book>> {
        // 获取单个书籍的逻辑...
    }

    async fn books(&self, ctx: &Context<'_>) -> FieldResult<Vec<Book>> {
        // 获取所有书籍的逻辑...
    }
}

struct Mutation;

#[Object]
impl Mutation {
    async fn create_book(
        &self,
        ctx: &Context<'_>,
        title: String,
        author: String,
    ) -> FieldResult<Book> {
        // 创建书籍的逻辑...
    }

    async fn update_book(
        &self,
        ctx: &Context<'_>,
        id: i32,
        title: Option<String>,
        author: Option<String>,
    ) -> FieldResult<Option<Book>> {
        // 更新书籍的逻辑...
    }

    async fn delete_book(&self, ctx: &Context<'_>, id: i32) -> FieldResult<bool> {
        // 删除书籍的逻辑...
    }
}

pub fn create_schema() -> Schema<Query, Mutation, EmptySubscription> {
    Schema::build(Query {}, Mutation {}, EmptySubscription::default())
        .finish()
}
```

## 3.配置服务 在主程序或服务器启动代码中，加载并使用上面创建的 Schema。

```rust
# main.rs 或类似的启动文件
use async_graphql::{EmptySubscription, Schema};
use crate::resolvers::{create_schema, Query, Mutation};

#[tokio::main]
async fn main() {
    let schema = create_schema();
    // 启动 GraphQL 服务，如使用 async-graphql-tide、async-graphql-warp 等库。
}
```

通过这样的组织结构，你可以清晰地看到每个资源（如 Book）的查询和变更操作是如何被组织到各自的模块和结构体中的，并且与 GraphQL Schema 保持一致。